const spel = {
    mogelijkeKeuzes: ["Steen", "Papier", "Schaar"],
    spelers: ["Speler 1", "Speler 2"],
    keuzes: [],
    huidigeSpelerIndex: 0,
};

setHuidigeSpeler(spel.huidigeSpelerIndex);

function kies(keuze) {
    spel.keuzes.push(keuze);
    spel.huidigeSpelerIndex++;
    if (spel.huidigeSpelerIndex > 1) {
        bepaalWinnaar();
        document.getElementById('huidigeSpeler').innerText = 'Goed gespeeld';
        document.getElementById('keuzes').style.display = 'none';
    } else {
        setHuidigeSpeler(spel.huidigeSpelerIndex);
    }
}

function bepaalWinnaar() {
    // vind de index van de keuze op basis van wat de student gekozen heeft
    const [indexKeuzeSpeler1, indexKeuzeSpeler2] = spel.keuzes
        .map(keuze => spel.mogelijkeKeuzes.findIndex(mogelijkeKeuze => mogelijkeKeuze === keuze));

    const uitkomstElement = document.getElementById('uitkomst');

    if (indexKeuzeSpeler1 === indexKeuzeSpeler2) {
        uitkomstElement.innerText = "Gelijkspel";
    } else if (indexKeuzeSpeler1 === (indexKeuzeSpeler2 + 1) % 3) {
        uitkomstElement.innerText = "Speler 1 heeft gewonnen";
    } else {
        uitkomstElement.innerText = "Speler 2 heeft gewonnen";
    }
}

function setHuidigeSpeler(spelerIndex) {
    document.getElementById('huidigeSpeler').firstChild.innerText = spel.spelers[spelerIndex];
}

function herstart() {
    location.reload();
}